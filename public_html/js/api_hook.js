const service = "http://host.bisswanger.com/dhbw/calendar.php";
const serviceID = "c7458bfd";

function listEvents(callback) {
    get("list", {}, callback);
}

function deleteEvent(id, callback) {
    post("delete", {id: id}, callback);
}

function createEvent(title, location, organizer, start, end, status, allday, webpage, callback) {
    var params = {
        title: title.substring(0, 50),
        location: location.substring(0, 50),
        organizer: organizer,
        status: status,
        start: start,
        end: end,
        allday: (allday) ? 1 : 0,
        webpage: webpage
    };
    post("add", params, callback);
}

function updateEvent(id, title, location, organizer, start, end, status, allday, webpage, callback) {
    var params = {
        id: id,
        title: title.substring(0, 50),
        location: location.substring(0, 50),
        organizer: organizer,
        status: status,
        start: start,
        end: end,
        allday: (allday) ? 1 : 0,
        webpage: webpage
    };
    post("update", params, callback);
}

function listCategories(callback) {
    get("list-categories", {}, callback);
}

function deleteCategory(id, callback) {
    post("delete-category", {id: id}, callback);
}

function createCategory(name, callback) {
    post("add-category", {name: name}, callback);
}

function addImage(id, file, callback) {
    var formData = new FormData();
    formData.append("file", file);
    formData.append("id", id);
    formData.append("action", "upload-image");
    formData.append("user", serviceID);
    $.ajax({
        url: service,
        dataType: "text",
        cache: false,
        contentType: false,
        processData: false,
        type: 'post',
        data: formData,
        success: function(data) {
            if(callback !== undefined) {
                callback(data);
            }
        }
    });
    //post("upload-image", {id: id, file: file}, callback);
}

function deleteImage(id, callback) {
    post("delete-image", {id: id}, callback);
}

function removeFromCategory(event, category, callback) {
    post("remove-category", {event: event, category: category}, callback);
}

function addToCategory(event, category, callback) {
    post("put-category", {event: event, category: category}, callback);
}

function get(action, additionalParams, callback) {
    var params = {user: serviceID, action: action, format: "json"};
    if(additionalParams !== undefined) {
        $.each(additionalParams, function(index, value) {
            if(value !== undefined) {
                params[index] = value;
            }
        });
    }
    $.get(service, params, function(data) {
        if(callback !== undefined) {
            callback(data);
        }
    }).fail(function() {
        alert("Connection error");
    });
}

function post(action, additionalParams, callback) {
    var params = {user: serviceID, action: action, format: "json"};
    if(additionalParams !== undefined) {
        $.each(additionalParams, function(index, value) {
            if(value !== undefined) {
                params[index] = value;
            }
        });
    }
    $.post(service, params, function(data) {
        if(callback !== undefined) {
            callback(data);
        }
    }).fail(function() {
        alert("Connection error");
    });
}

//DEBUG
function clearAll() {
    listEvents(function(data) {
        $.each(data.events.events, function(i, item) {
            deleteEvent(item.id, function(event) {
                if (!event.hasOwnProperty("error")) {
                    console.log("Deleted: " + item.id);
                }
                else {
                    console.log("Could not delete: " + item.id);
                }
            });
        });
    });
}

function dateToString(date) {
    var formatted = "Y-M-DTh:m";
    return formatted.replace("Y", date.getFullYear())
            .replace("M", date.getMonth() + 1)
            .replace("D", date.getDate())
            .replace("h", date.getHours())
            .replace("m", date.getMinutes())
            .replace(/(\D)(\d)(\D|$)/g, "$10$2$3");
}