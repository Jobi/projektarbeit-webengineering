const calendarEntryID = "calendarEntryData";
        const aTemplate = "href=\"%webpage%\"";
        const imgTemplate = "<td rowspan=\"3\" class=\"calenderEntryPic\"><img src=\"%picture%\"></td>\n";
        const entryTemplate =
        "<li id=\"id_%id%\" class=\"calendarEntry %status%\" category=\"%category%\">\n" +
        "   <a %aTemplate%>" +
        "       <table class=\"calenderEntryTable\">\n" +
        "           <tr>\n" +
        "               <td class=\"calenderEntryDate\">%dateStart% - %timeStart%</td>\n" +
        "              <th class=\"calenderEntryTitle\">%title%</th>\n" +
        "               %imgTemplate%" +
        "           </tr>\n" +
        "          <tr>\n" +
        "               <td class=\"calenderEntryTime\">%dateEnd% - %timeEnd%</td>\n" +
        "               <td rowspan=\"2\" class=\"calenderEntryOrganizer\">%organizer%</td>\n" +
        "          </tr>\n" +
        "           <tr>\n" +
        "               <td class=\"calenderEntryLocation\">%location%</td>\n" +
        "          </tr>\n" +
        "       </table>\n" +
        "   </a>\n" +
        "</li>";

var displayLimit = 5;
var storeLimit = 100;
var currentEntries = [];

function insertEntry(time, entry, id) {
    for (var i = 0; i < displayLimit || (i < currentEntries.length && i < storeLimit); i++) {
        if (i >= currentEntries.length || currentEntries[i][0] > time) {
            currentEntries.splice(i, 0, [time, entry, id]);
            break;
        }
    }
}

function appendEntry(title, dateStart, timeStart, dateEnd, timeEnd, organizer, location, status, webpage, id, category, picture) {
    var entry = entryTemplate;
    if (id == undefined) {
        id = getRandomInt(1000000, 10000000);
    }
    if (picture == undefined) {
        entry = entry.replace("%imgTemplate%", "");
    }
    else {
        entry = entry.replace("%imgTemplate%", imgTemplate);
    }
    if (webpage == "" || webpage == undefined) {
        entry = entry.replace("%aTemplate%", "");
    }
    else {
        entry = entry.replace("%aTemplate%", aTemplate);
    }
    if (category == undefined) {
        entry = entry.replace("%category%", "default");
    }
    entry = entry.replace("%id%", id).replace("%title%", title).replace("%dateStart%", dateStart).replace("%timeStart%", timeStart).replace("%dateEnd%", dateEnd).replace("%timeEnd%", timeEnd).replace("%picture%", picture).replace("%organizer%", organizer).replace("%location%", location).replace("%webpage%", webpage).replace("%status%", status).replace("%category%", category);
    insertEntry(dateStart + timeStart, entry, id);
    updateDisplay();
}

function removeEntry(id) {
    var i;
    for (i = 0; i < currentEntries.length; i++) {
        if (currentEntries[i][2] == id)
            break;
    }
    currentEntries.splice(i, 1);
    updateDisplay();
}

function updateDisplay() {
    clearEntries();
    for (var i = 0; i < displayLimit && i < currentEntries.length; i++) {
        $("#" + calendarEntryID).append(currentEntries[i][1]);
    }
}

function clearEntries() {
    $("#" + calendarEntryID).empty();
}

function forceUpdate() {
    currentEntries = [];
    loadEntries();
}

function loadEntries() {
    clearEntries();
    listEvents(function (data) {
        $.each(data.events.events, function (i, item) {
            if (item.categories.length == 0) {
                if(item.imageurl !== "") {
                    appendEntry(item.title, stringToDate(item.start), stringToTime(item.start), stringToDate(item.end), stringToTime(item.end), item.organizer, item.location, item.status, item.webpage, item.id, undefined, item.imageurl);
                }
                else {
                    appendEntry(item.title, stringToDate(item.start), stringToTime(item.start), stringToDate(item.end), stringToTime(item.end), item.organizer, item.location, item.status, item.webpage, item.id);
                }
            }
            else {
                if (item.imageurl !== "") {
                    appendEntry(item.title, stringToDate(item.start), stringToTime(item.start), stringToDate(item.end), stringToTime(item.end), item.organizer, item.location, item.status, item.webpage, item.id, item.categories[0]['name'], item.imageurl);
                }
                else {
                    appendEntry(item.title, stringToDate(item.start), stringToTime(item.start), stringToDate(item.end), stringToTime(item.end), item.organizer, item.location, item.status, item.webpage, item.id, item.categories[0]['name']);
                }
            }
        });
    });
}

function requestEventCreation(title, location, organizer, start, end, status, allday, webpage, category) {
    if (webpage == "")
        webpage = undefined;
    createEvent(title, location, organizer, start, end, status, allday, webpage, function (data) {
        if (!data.hasOwnProperty("error")) {
            if (category !== undefined) {
                addToCategory(data.add.id, getID(category), function (categoryData) {
                    if (!categoryData.hasOwnProperty("error")) {
                        appendEntry(title, stringToDate(start), stringToTime(start), stringToDate(end), stringToTime(end), organizer, location, status, webpage, data.add.id, category);
                    }
                });
            }
            else {
                appendEntry(title, stringToDate(start), stringToTime(start), stringToDate(end), stringToTime(end), organizer, location, status, webpage, data.add.id);
            }
        }
        else {
            alert("ERROR:\n"+data.error.text);
            console.log(data);
        }
    });
}

function editEvent(id, title, location, organizer, start, end, status, allday, webpage, category) {
    if (webpage == "")
        webpage = undefined;
    updateEvent(id, title, location, organizer, start, end, status, allday, webpage, function (data) {
        if (!data.hasOwnProperty("error")) {
            if (category !== undefined) {
                addToCategory(id, getID(category), function (categoryData) {
                    if (!categoryData.hasOwnProperty("error")) {
                        forceUpdate();
                    }
                });
            }
            else {
                forceUpdate();
            }
        }
        else {
            alert("ERROR:\n"+data.error.text);
            console.log(data);
        }
    });
}

function requestEventDeletion(id) {
    deleteEvent(id, function (data) {
        if (!data.hasOwnProperty("error")) {
            removeEntry(id);
        }
    });
}

function requestAddPicture(id, file) {
    addImage(id, file, function (data) {
        if (!data.hasOwnProperty("error")) {
            forceUpdate();
        } else {
            console.log(data);
        }
    });
}

function requestRemovePicture(id) {
    deleteImage(id, function (data) {
        if (!data.hasOwnProperty("error")) {
            forceUpdate();
        } else {
            console.log(data);
        }
    });
}

function stringToDate(string) {
    var date = string.split("T")[0].split("-");
    return date[2].concat(".").concat(date[1]).concat(".").concat(date[0]);
}

function stringToTime(string) {
    return string.split("T")[1];
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
