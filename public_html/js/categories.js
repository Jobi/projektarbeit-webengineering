$(document).ready(function () {
    loadCategories();
});

var observer = new MutationObserver(function() {
    if (document.getElementById('calendarEntryData')) {
        var children = $('#calendarEntryData').children();
        for(var i = 0; i < children.length; i++) {
            applyCategory(children[i]);
        }
    }
});
observer.observe(document, {
    childList: true,
    subtree: true,
    attributes: true,
    characterData: false,
});

var categories = [];

function loadCategories() {
    listCategories(function(data) {
        categories = data.categories.categories;
    });
}

function getID(name) {
    for(var i = 0; i < categories.length; i++) {
        if(categories[i]['name'] == name) {
            return categories[i]['id'];
        }
    }
}

function getCategories() {
    var names = [];
    for(var i = 0; i < categories.length; i++) {
        names[i] = categories[i]['name'];
    }
    return names;
}

function addCategory(name) {
    createCategory(name, function(data) {
        categories[categories.length] = {name: name, id: data.addcategory.id};
    });
}

function removeCategory(name) {
    for(var i = 0; i < categories.length; i++) {
        if(categories[i]['name'] == name) {
            deleteCategory(categories[i]['id'], function() {
                forceUpdate();
                categories.splice(i, 1);
            });
            break;
        }
    }
}

function setCategory(id, name) {
    addToCategory(id, getID(name), function() {
        var element = $('#id_' + id)[0];
        element.setAttribute("category", name);
        applyCategory(element);
    });
}

function unsetCategory(id, name) {
    removeFromCategory(id, getID(name), function() {
        forceUpdate();
    });
}

function applyCategory(node) {
    var category = node.getAttribute("category");
    if(category != null && category != 'default') {
        node.style.color = getTextColor(category);
        node.style.backgroundColor = getBackgroundColor(category);
    }
    else {
        node.style.color = null;
        node.style.backgroundColor = null;
    }
}

function getTextColor(name) {
    return '#' + intToRGB(hashCode(name) ^ 0xFFFFFF);
}

function getBackgroundColor(name) {
    return '#' + intToRGB(hashCode(name));
}

function hashCode(str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
}

function intToRGB(i){
    var c = (i & 0x00FFFFFF)
        .toString(16)
        .toUpperCase();
    return "00000".substring(0, 6 - c.length) + c;
}