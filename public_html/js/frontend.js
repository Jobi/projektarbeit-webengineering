$(document).ready(function () {
    //------------------------RightClickMenu--------------------------------\\
    var boolChangeAdd;
    var currentID;
    //Show RightClickMenu
    document.addEventListener("contextmenu", function (e) {
        //if rightclick on Calenderentry, set currentID to its ID
        if (currentID = isCalenderEntry(e, "calendarEntry")) {
            showRightClick(e);
            e.preventDefault();
        }
    }, false);

    //Hide RightClickMenu
    $("body").on("click", function () {
        if ($("#rightClick")[0].style.display === "block" && !mouseIsOver("#rightClick")) {
            $("#rightClick")[0].style.display = "none";
        }
    });
    //Perform RightClick Action
    $("#rightClick li").on("click", function () {
        $("#rightClick")[0].style.display = "none";
        switch ($(this).attr("data-action")) {
            case "edit":
                //set values for Input Form
                boolChangeAdd = "change";
                $("input[id='title']").val($(currentID).find(".calenderEntryTitle").html());
                $("input[id='location']").val($(currentID).find(".calenderEntryLocation").html());
                $("input[id='organizer']").val($(currentID).find(".calenderEntryOrganizer").html());
                $("input[id='startDateTime']").val(
                        $(currentID).find(".calenderEntryDate").html().split(".")[2].split(" - ")[0]
                        + "-" + $(currentID).find(".calenderEntryDate").html().split(".")[1]
                        + "-" + $(currentID).find(".calenderEntryDate").html().split(".")[0]
                        + "T" + $(currentID).find(".calenderEntryDate").html().split(" - ")[1]);
                $("input[id='endDateTime']").val(
                        $(currentID).find(".calenderEntryTime").html().split(".")[2].split(" - ")[0]
                        + "-" + $(currentID).find(".calenderEntryTime").html().split(".")[1]
                        + "-" + $(currentID).find(".calenderEntryTime").html().split(".")[0]
                        + "T" + $(currentID).find(".calenderEntryTime").html().split(" - ")[1]);
                $("select[id='status']").val($(currentID).hasClass("Busy") ? "Busy" : $(currentID).hasClass("Free") ? "Free" : "Tentative");
                $("input[id='link']").val($(currentID).find("a").attr("href"));

                toggleVisibilty("#addEntry");
                toggleVisibilty("#coverback");
                break;
            case "remove":
                requestEventDeletion(parseInt(currentID.id.split("_")[1]));
                break;
            case "category":
                showManageCategory();
                break;
            case "picture":
                toggleVisibilty("#addPicture");
                toggleVisibilty("#coverback");
                break;
            case "remove_img":
                requestRemovePicture(currentID.id.split("_")[1]);
                break;
        }
    });

    //------------------------NavigationAddEntry----------------------------\\
    $("#navTabaddEntry").on("click", function () {
        //clear Input Form
        boolChangeAdd = "add";
        $("input[id='title']").val("");
        $("input[id='location']").val("");
        $("input[id='organizer']").val("");
        $("input[id='startDateTime']").val("");
        $("input[id='endDateTime']").val("");
        $("select[id='status']").val("");
        $("input[id='link']").val("");
        toggleVisibilty("#addEntry");
        toggleVisibilty("#coverback");
    });
    $("#addCancel").on("click", function () {
        toggleVisibilty("#addEntry");
        toggleVisibilty("#coverback");
    });

    $("#addEntry").on("submit", function (e) {
        e.preventDefault();
        var start = $("#allday")[0].checked ? $("#startDateTime").val().split("T")[0].concat("T00:00") : $("#startDateTime").val(); //If Allday: set time to 00:00
        var end = $("#allday")[0].checked ? $("#endDateTime").val().split("T")[0].concat("T23:59") : $("#endDateTime").val(); //If Allday: set time to 23:59
        if (start >= end) {
            alert("End time has to be after start time!");
        } else {
            if (boolChangeAdd === "add") {
                requestEventCreation(
                        $("#title").val(),
                        $("#location").val(),
                        $("#organizer").val(),
                        start,
                        end,
                        $("#status").val(),
                        $("#allday")[0].checked ? 1 : 0,
                        $("#link").val() === "" ? null : $("#link").val()
                        );
            } else {
                editEvent(
                        parseInt(currentID.id.split("_")[1]),
                        $("#title").val(),
                        $("#location").val(),
                        $("#organizer").val(),
                        start,
                        end,
                        $("#status").val(),
                        $("#allday")[0].checked ? 1 : 0,
                        $("#link").val() === "" ? null : $("#link").val()
                        );
            }
            toggleVisibilty("#addEntry");
            toggleVisibilty("#coverback");
        }
    });

    //------------------------ManageCategories-----------------------------\\
    //Add new Category
    $("#addCategory").on("click", function (e) {
        e.preventDefault();
        if ($("#categoryName").val() !== "") {
            addCategory($("#categoryName").val());
            $("input[id='categoryName']").val("");
            toggleVisibilty("#manageCategory");
            toggleVisibilty("#coverback");
        }
    });
    //Remove existing Category
    $("#removeCategory").on("click", function (e) {
        e.preventDefault();
        if ($("#removeCategories").val() !== "") {
            removeCategory($("#removeCategories").val());
            toggleVisibilty("#manageCategory");
            toggleVisibilty("#coverback");
        }
    });
    //Attach existing Category to Event
    $("#manageCategory").on("submit", function (e) {
        toggleVisibilty("#manageCategory");
        toggleVisibilty("#coverback");
        e.preventDefault();
        setCategory(
                parseInt(currentID.id.split("_")[1]),
                $("#currentCategories").val()
                );
    });

    $("#categoryCancel").on("click", function () {
        toggleVisibilty("#manageCategory");
        toggleVisibilty("#coverback");
    });

    //------------------------Pictures-----------------------------\\
    $("#addPicture").on("submit", function (e) {
        var file_data = $("#choosePic").prop('files')[0];
        e.preventDefault();
        requestAddPicture(currentID.id.split("_")[1], file_data);
        toggleVisibilty("#addPicture");
        toggleVisibilty("#coverback");
    });

    $("#pictureCancel").on("click", function () {
        toggleVisibilty("#addPicture");
        toggleVisibilty("#coverback");
    });
});

//---------------------------Functions-------------------------------------\\
function toggleVisibilty(string) {
    $(string).toggleClass("visible");
}

function showRightClick(event) {
    $("#rightClick")[0].style.display = "block";
    $("#rightClick")[0].style.top = event.clientY + "px";
    $("#rightClick")[0].style.left = event.clientX + "px";
}
function showManageCategory() {
    reloadCategories();
    toggleVisibilty("#manageCategory");
    toggleVisibilty("#coverback");
}
function reloadCategories() {
    var categories = getCategories();
    if (categories.length === 0) {
        $("#currentCategories").attr("disabled", true);
        $("#removeCategories").attr("disabled", true);

    } else {
        $("#removeCategories").attr("disabled", false);
        $("#currentCategories").attr("disabled", false);
        $("#removeCategories").html("");
        $("#currentCategories").html("");
        for (var i = 0; i < categories.length; i++) {
            $("#currentCategories").append("<option>" + categories[i] + "</option>");
            $("#removeCategories").append("<option>" + categories[i] + "</option>");
        }
    }
}

function isCalenderEntry(e, className) {
    var temp = e.srcElement || e.target;

    if (temp.classList.contains(className)) {
        return temp;
    } else {
        //if current element isn't calenderetry, go to parent and check it
        while (temp = temp.parentNode) {
            if (temp.classList && temp.classList.contains(className)) {
                return temp;
            }
        }
    }
    return false;
}

function mouseIsOver(what) {
    var test = $(what).filter(":hover");
    return test.length >= 1;
}